Modes:
- endless rounds until one player remains
- race to finish # of rounds (start new round immediately)
- best timed high score (wait for all to finish)
- player pvp tag-all (first to tag everyone)
