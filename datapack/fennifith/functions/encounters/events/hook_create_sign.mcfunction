execute if entity @s[tag=min.sign] unless entity @e[tag=enc.sign] run tag @s add enc.sign
execute if entity @s[tag=enc.sign] run tag @s remove min.sign
execute if entity @s[tag=enc.sign] at @s run data modify block ~ ~ ~ Text2 set value '{"text":"Endless","clickEvent":{"action":"run_command","value":"execute unless score #global_stop min.args matches 1 run function fennifith:encounters/events/hook_start"}}'
execute if entity @s[tag=enc.sign] at @s run data modify block ~ ~ ~ Text3 set value '{"text":"Encounters"}'
execute if entity @s[tag=enc.sign] at @s run setblock ~ ~3 ~ bell[attachment=ceiling]
