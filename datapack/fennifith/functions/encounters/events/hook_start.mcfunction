# start control of the server
scoreboard players set #global_running enc.args 1
execute in fennifith:encounters run function fennifith:encounters/events/load

execute in fennifith:encounters run function fennifith:encounters/game/stop
execute in fennifith:encounters run function fennifith:encounters/game/set_structures

# tp all players to encounters dimension
execute in fennifith:encounters run tp @a <<config.spawnpoint.encounters>>

# show game menu
function fennifith:encounters/game/do_menu

# say DEBUG: start encounters
