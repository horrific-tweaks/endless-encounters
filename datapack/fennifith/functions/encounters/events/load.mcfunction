scoreboard objectives add min.args dummy

scoreboard objectives add enc.is_death deathCount

scoreboard objectives add enc.tmp dummy
scoreboard objectives add enc.args dummy
scoreboard objectives add enc.player dummy
scoreboard objectives add enc.mobs dummy
scoreboard objectives add enc.round dummy
scoreboard objectives add enc.deaths dummy
scoreboard objectives add enc.arrows dummy
scoreboard objectives add enc.reload_cooldown dummy

scoreboard objectives add enc.points dummy "Score"

# start respawn loop schedule
schedule function fennifith:encounters/events/loop_respawn 10s append

# set up gamerules
execute in fennifith:encounters run difficulty normal

execute in fennifith:encounters run time set noon
execute in fennifith:encounters run weather clear
gamerule doMobSpawning true
gamerule doDaylightCycle false
gamerule doWeatherCycle false
gamerule doInsomnia false
gamerule doImmediateRespawn true
gamerule keepInventory true
