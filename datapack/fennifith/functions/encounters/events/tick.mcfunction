# give taggable entities a constant glowing effect
effect give @e[tag=enc.mob_taggable] glowing 1

# if a player has a death score, run on_respawn
execute as @a[tag=enc.player,scores={enc.is_death=1..}] run function fennifith:encounters/game/on_respawn
scoreboard players reset @a enc.is_death

# if a player is in the wrong dimension, run on_respawn
execute as @a[tag=enc.player] unless data entity @s {Dimension:"fennifith:encounters"} run function fennifith:encounters/game/on_respawn

# remove all slime spawns
tp @e[type=slime] 0 -256 0

# update player scores
execute if entity @a[tag=enc.player] run function fennifith:encounters/game/set_points
