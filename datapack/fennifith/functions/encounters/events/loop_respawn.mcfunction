# give all non-players saturation effects
effect give @a[tag=!enc.player] saturation 10 100 true
effect give @a[tag=enc.player,gamemode=!survival] saturation 10 100 true

# update player respawn locations
execute as @a[tag=!enc.player] in fennifith:encounters run spawnpoint @s <<config.spawnpoint.encounters>>
execute as @a[tag=enc.player,nbt={Dimension:"fennifith:encounters"}] at @s run spawnpoint @s ~ ~ ~

# forceload all player chunks
execute in fennifith:encounters run forceload remove all
execute in fennifith:encounters run forceload add 0 0
execute as @e[tag=enc.player,nbt={Dimension:"fennifith:encounters"}] at @s run forceload add ~ ~

# check mobs count for active players
<% for i in range(0, config.players) %>
execute as @a[tag=enc.player,scores={enc.player=<<i>>}] run execute store result score @s enc.tmp if entity @e[tag=enc.mob_taggable,scores={enc.player=<<i>>}]
<% endfor %>
#   if there are less current mobs than expected mobs, run spawning function
execute as @a[tag=enc.player,gamemode=survival] if score @s enc.tmp < @s enc.mobs at @s run function fennifith:encounters/game/do_spawning

# reschedule spawn loop
execute if score #global_running enc.args matches 1 run schedule function fennifith:encounters/events/loop_respawn 10s replace
