# reset game
function fennifith:encounters/game/stop
function fennifith:encounters/game/set_structures

scoreboard objectives setdisplay sidebar enc.points
advancement grant @s only fennifith:encounters/root

# set up tags for player slots
<% for i in range(0, config.players) %>
execute as @a[tag=!enc.player,limit=1] run scoreboard players set @s enc.player <<i>>
tag @a[scores={enc.player=<<i>>}] add enc.player
<% endfor %>

scoreboard players set @a enc.round 0
# set seed for deciding mobs to spawn
execute store result score #seed enc.args run loot spawn ~ -65 ~ loot fennifith:encounters/random_seed
execute positioned ~ -65 ~ run kill @e[type=item,distance=..1,nbt={Item:{id:"minecraft:dirt"}}]

<% for i in range(0, config.players) %>
bossbar add enc.bossbar<<i>> "Enemies Remaining"
bossbar set enc.bossbar<<i>> color red
bossbar set enc.bossbar<<i>> style progress
bossbar set enc.bossbar<<i>> visible true
bossbar set enc.bossbar<<i>> players @a[scores={enc.player=<<i>>}]
<% endfor %>

# set up player inventory
execute as @a[tag=enc.player] run function fennifith:encounters/game/set_inventory

# tp players to start locations
execute in fennifith:encounters run tp @a[tag=enc.player] <<config.spawnpoint.encounters>>
execute in fennifith:encounters run spreadplayers 0 0 10000 90000 under 63 false @a[tag=enc.player]
schedule function fennifith:encounters/game/on_game_start 5s replace
gamemode survival @a[tag=enc.player]

# announce gamemode setting
<% for modekey, mode in gamemodes %>
# if gamemode: <<modekey>>
<% set if_gamemode %>if score #global_gamemode enc.args matches <<loop.index>><% endset %>

execute <<if_gamemode>> run tellraw @a [{"text":"Current gamemode - <<modekey>>!","color":"aqua"}]
execute <<if_gamemode>> run tellraw @a [{"text":"<<mode.description>>","italic":true,"color":"gray"}]
<% if mode.game_end == "round_limit" %>
execute <<if_gamemode>> run tellraw @a [{"text":"The game will end after "},{"score":{"name":"#global_round_limit","objective":"enc.args"},"color":"aqua"},{"text":" rounds","color":"aqua"},{"text":"."}]
<% endif %>
<% endfor %>
