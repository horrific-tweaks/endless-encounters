tag @s remove enc.player_round
scoreboard players add @s enc.round 1

# if won, kill all enemies
<% for i in range(0, config.players) %>
execute if entity @s[scores={enc.player=<<i>>}] run kill @e[tag=enc.mob,scores={enc.player=<<i>>}]
<% endfor %>

# start new round
<% for modekey, mode in gamemodes %>
# if gamemode: <<modekey>>
<% set if_gamemode %>if score #global_gamemode enc.args matches <<loop.index>><% endset %>

# determine whether to end game
<% if mode.game_end == "no_player_remains" %>
execute <<if_gamemode>> unless entity @a[tag=enc.player,gamemode=survival] run function fennifith:encounters/game/on_game_end
<% elif mode.game_end == "round_limit" %>
execute <<if_gamemode>> if score @s enc.round >= #global_round_limit enc.args run function fennifith:encounters/game/on_game_end
<% elif mode.game_end == "round_limit_all" %>
# only end the game if there are no longer any players below the round limit
execute <<if_gamemode>> run scoreboard players set #is_playing enc.tmp 0
execute <<if_gamemode>> as @a[tag=enc.player] if score @s enc.round < #global_round_limit enc.args run scoreboard players set #is_playing enc.tmp 1
execute <<if_gamemode>> if score #is_playing enc.tmp matches 0 run function fennifith:encounters/game/on_game_end
<% elif mode.game_end == "always" %>
execute <<if_gamemode>> run function fennifith:encounters/game/on_game_end
<% endif %>

# determine whether to start next round (if the game is still running; implied by tag=enc.player)
<% if mode.round_start == "all" %>
# start only once everyone has finished the round
execute <<if_gamemode>> if entity @a[tag=enc.player_round] run tellraw @s {"text":"Waiting for other players to finish the round...","bold":true,"color":"aqua"}
execute <<if_gamemode>> unless entity @a[tag=enc.player_round] as @a[tag=enc.player] run function fennifith:encounters/game/on_round_start
<% else %>
# start players individually
execute <<if_gamemode>> as @s[tag=enc.player] run function fennifith:encounters/game/on_round_start
<% endif %>
<% endfor %>
