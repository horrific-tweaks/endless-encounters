# if a player is somehow in the wrong dimension, teleport to 0,0
execute unless data entity @s {Dimension:"fennifith:encounters"} in fennifith:encounters run tp @s <<config.spawnpoint.encounters>>

# add to respawn score
scoreboard players add @s enc.deaths 1

<% for modekey, mode in gamemodes %>
# if gamemode: <<modekey>>
<% set if_gamemode %>if score #global_gamemode enc.args matches <<loop.index>><% endset %>

<% if not mode.respawn %>
execute <<if_gamemode>> run gamemode spectator @s
<% endif %>
<% endfor %>

# reset player inventory
function fennifith:encounters/game/set_inventory

# temporary resistance after respawn (5s)
effect give @s minecraft:resistance 5 100 true
execute at @s run playsound minecraft:item.totem.use player @s
execute at @s run particle minecraft:totem_of_undying ~ ~ ~ 0.25 0.25 0.25 1 100 normal @s

# if gamemode ends on no_player_remains, check whether to end the game
<% for modekey, mode in gamemodes %>
# if gamemode: <<modekey>>
<% set if_gamemode %>if score #global_gamemode enc.args matches <<loop.index>><% endset %>

# determine whether to end game
<% if mode.game_end == "no_player_remains" %>
execute <<if_gamemode>> unless entity @a[tag=enc.player,gamemode=survival] run function fennifith:encounters/game/on_game_end
<% endif %>
<% endfor %>
