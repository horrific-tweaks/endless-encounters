# sum points for current gamemode
scoreboard players set @s enc.points 0

<% for modekey, mode in gamemodes %>
# if gamemode: <<modekey>>
<% set if_gamemode %>if score #global_gamemode enc.args matches <<loop.index>><% endset %>

<% for source in mode.points %>
# store tmp points for scale
execute <<if_gamemode>> run scoreboard players operation #tmp_<<source.id>> enc.args = @s <<source.id>>
<% if source.scale %>
execute <<if_gamemode>> run scoreboard players set #tmp_<<source.id>>_scale enc.args <<source.scale>>
execute <<if_gamemode>> run scoreboard players operation #tmp_<<source.id>> enc.args *= #tmp_<<source.id>>_scale enc.args
<% endif %>
# add scaled points to score
execute <<if_gamemode>> run scoreboard players operation @s enc.points += #tmp_<<source.id>> enc.args
<% endfor %>
<% endfor %>
