# determine mob count -> #mob_count enc.args
scoreboard players operation #mob_count enc.args = @s enc.mobs
#   count existing mobs spawned for the player
scoreboard players set #mob_count_current enc.args 0
<% for i in range(0, config.players) %>
execute if entity @s[scores={enc.player=<<i>>}] run execute store result score #mob_count_current enc.args if entity @e[tag=enc.mob_taggable,scores={enc.player=<<i>>}]
<% endfor %>
#   subtract from # of mobs to spawn
scoreboard players operation #mob_count enc.args -= #mob_count_current enc.args

# determine mob type -> #mob_type enc.args
scoreboard players set #mob_type_max enc.args <<mobs|length>>
scoreboard players operation #mob_type enc.args = @s enc.round
scoreboard players operation #mob_type enc.args += #seed enc.args
scoreboard players operation #mob_type enc.args %= #mob_type_max enc.args

# summon mobs at ~ 128 ~
<% for mob, mobinfo in mobs %>
<% set mob_type = loop.index0 %>
<% for mob_count in range(1, config.mob_count.max+1) %>
execute if score #mob_type enc.args matches <<mob_type>> if score #mob_count enc.args matches <<mob_count>>.. positioned ~ 128 ~ run summon <<mob>>
<% endfor %>
<% endfor %>

# add tags to summoned entities
execute positioned ~ 128 ~ run tag @e[distance=..1] add enc.mob
execute positioned ~ 128 ~ run tag @e[distance=..1] add enc.mob_spawning
execute positioned ~ 128 ~ run tag @e[distance=..1] add enc.mob_taggable
scoreboard players operation @e[tag=enc.mob_spawning] enc.player = @s enc.player

# spread summoned entities around location, restricting below known y-pos
spreadplayers ~ ~ 3 20 under 63 false @e[tag=enc.mob_spawning]
# give entities weakness/slowness for 5 secs, for reaction time
effect give @e[tag=enc.mob_spawning] weakness 5 100
effect give @e[tag=enc.mob_spawning] slowness 5 100

# play teleport sound effect
execute as @e[tag=enc.mob_spawning] at @s run playsound minecraft:entity.enderman.teleport player @a ~ ~ ~
execute as @e[tag=enc.mob_spawning] at @s run particle minecraft:cloud ~ ~0.5 ~ 1 1 1 0 10

# remove currently-spawning tags
tag @e remove enc.mob_spawning
