# increment round scoreboard
tellraw @s [{"text":"Round ","color":"green"},{"score":{"name":"@s","objective":"enc.round"}},{"text":"..."}]

# calculate mob count between [min, max] -> #mob_count enc.args
scoreboard players set #mob_count_max enc.args << config.mob_count.max - config.mob_count.min >>
scoreboard players operation #mob_count enc.args = @s enc.round
scoreboard players operation #mob_count enc.args %= #mob_count_max enc.args
scoreboard players add #mob_count enc.args <<config.mob_count.min>>

# set encounter_tag score for @s
scoreboard players operation @s enc.mobs = #mob_count enc.args
tag @s add enc.player_round

# spawn # of mobs around player
execute as @s[gamemode=survival] at @s run function fennifith:encounters/game/do_spawning

# play raid horn noise
<% set pitches = [".8", ".5", ".7", ".9", ".6", "1"] %>
scoreboard players set #pitch_max enc.args <<pitches|length>>
scoreboard players operation #pitch enc.args = @s enc.round
scoreboard players operation #pitch enc.args %= #pitch_max enc.args
<% for pitch in pitches %>
execute if score #pitch enc.args matches <<loop.index0>> run playsound minecraft:entity.wither.spawn hostile @s ~ ~ ~ 0.3 <<pitch>>
<% endfor %>

# set bossbar max/value
<% for i in range(0, config.players) %>
execute if entity @s[scores={enc.player=<<i>>}] store result bossbar enc.bossbar<<i>> max run scoreboard players get @s enc.mobs
execute if entity @s[scores={enc.player=<<i>>}] store result bossbar enc.bossbar<<i>> value run scoreboard players get @s enc.mobs
<% endfor %>

# restart respawn loop if stopped
function fennifith:encounters/events/loop_respawn
