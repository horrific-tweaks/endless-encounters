---
collection: "inventory"
filename: "on_inventory_<<key>>_reload.mcfunction"
---

<% for i in range(0, 9) %>
execute as @a[tag=enc.player] if data entity @s {Inventory:[{Slot:<<i>>b,id:"minecraft:<<key>>"}]} run item replace entity @s hotbar.<<i>> with << key >><< item.nbt | default({}) | dump_nbt >> << item.count | default(1) >>
<% endfor %>
