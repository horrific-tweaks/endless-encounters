---
collection: "inventory"
filename: "on_inventory_<<key>>.mcfunction"
---

schedule function fennifith:encounters/game/on_inventory_<<key>>_reload 1t append

<% if key == "crossbow" %>
# if used crossbow, add to arrows score
scoreboard players add @s enc.arrows 1
<% endif %>

# Reset the crossbow detection advancement
advancement revoke @s only fennifith:encounters/used_<<key>>
