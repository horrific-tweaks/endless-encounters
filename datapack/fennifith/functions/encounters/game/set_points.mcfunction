# reset all points scoreboards
scoreboard players reset * enc.points
# calculate individual player scores
execute as @a[tag=enc.player] run function fennifith:encounters/game/set_points_player

# get minimum points score
scoreboard players set #min_score enc.tmp 0
execute as @a[tag=enc.player] run scoreboard players operation #min_score enc.tmp < @s enc.points
# if min points is less than 0, add difference to all points
scoreboard players set #negative enc.tmp -1
scoreboard players operation #min_score enc.tmp *= #negative enc.tmp
execute if score #min_score enc.tmp matches 0.. run scoreboard players operation @a[tag=enc.player] enc.points += #min_score enc.tmp
