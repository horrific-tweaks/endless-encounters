function fennifith:encounters/game/set_points
execute as @a[tag=enc.player] run function fennifith:encounters/game/do_stats
execute as @a[tag=enc.player] run function fennifith:encounters/game/do_menu

clear @a[tag=enc.player]
gamemode adventure @a

tag @a remove enc.player
tag @a remove enc.player_round
kill @e[tag=enc.mob]

execute in fennifith:encounters run tp @a <<config.spawnpoint.encounters>>
playsound ui.toast.challenge_complete player @a <<config.spawnpoint.encounters>>

# figure out first/second/third players
execute as @a run scoreboard players operation @s enc.tmp = @s enc.points

<% for i in range(1, 4) %>
tag @a remove enc.winner<<i>>
tag @a remove enc.winner_select<<i>>

scoreboard players set #tmp_max enc.args -1
# store max score in enc.tmp -> #tmp_max enc.args
execute as @a run scoreboard players operation #tmp_max enc.args > @s enc.tmp
# finding player with the max score, give winner tag & reset enc.tmp
execute as @a if score #tmp_max enc.args <= @s enc.tmp if score @s enc.tmp matches 0.. run tag @s add enc.winner_select<<i>>
# ensure that only one player has the enc.winner_i tag
execute as @a[tag=enc.winner_select<<i>>,limit=1] run tag @s add enc.winner<<i>>
tag @a remove enc.winner_select<<i>>

# set head on armor stand
execute as @a[tag=enc.winner<<i>>] if score @s enc.tmp matches 0.. run loot replace entity @e[type=armor_stand,tag=enc.place<<i>>,limit=1] armor.head loot fennifith:encounters/player_head
execute as @a[tag=enc.winner<<i>>] run scoreboard players set @s enc.tmp -1
<% endfor %>

# reset player spawn points to overworld
function fennifith:encounters/events/loop_respawn

# remove all bossbars
<% for i in range(0, config.players) %>
bossbar remove enc.bossbar<<i>>
<% endfor %>
