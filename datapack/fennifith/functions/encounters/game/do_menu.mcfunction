<% set gamemode_buttons %>
<% for modekey, mode in gamemodes %>
{"text":" [<<modekey|title>>] ","color":"green","clickEvent":{"action":"run_command","value":"/scoreboard players set #global_gamemode enc.args <<loop.index>>"},"hoverEvent":{"action":"show_text","value":"<<mode.description>>"}}<% if not loop.last %>,<% endif %>
<% endfor %>
<% endset %>

<% set round_buttons %>
<% for num in [5, 10, 20, 50] %>
{"text":" [<<num>> Rounds] ","color":"green","clickEvent":{"action":"run_command","value":"/scoreboard players set #global_round_limit enc.args <<num>>"}}<% if not loop.last %>,<% endif %>
<% endfor %>
<% endset %>

tellraw @s [{"text":"Choose gamemode: ","color":"gray"},<< gamemode_buttons | trim | replace("\n", "") >>]
tellraw @s [{"text":"# rounds: ","color":"gray"},<< round_buttons | trim | replace("\n", "") >>]
tellraw @s [{"text":"[Start]","color":"aqua","bold":true,"clickEvent":{"action":"run_command","value":"/function fennifith:encounters/game/start"}}]
