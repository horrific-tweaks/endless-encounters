# subtract one from remaining entities to be tagged
scoreboard players remove @s enc.mobs 1

# update bossbar value
<% for i in range(0, config.players) %>
execute if entity @s[scores={enc.player=<<i>>}] store result bossbar enc.bossbar<<i>> value run scoreboard players get @s enc.mobs
<% endfor %>

# locate tagged entity using HurtTime
tag @e remove enc.mob_target
execute as @e[tag=enc.mob_taggable] if data entity @s {HurtTime:10s} run tag @s add enc.mob_target
# if no hurt entity found, tag the nearest entity instead
execute unless entity @e[tag=enc.mob_target] run tag @e[tag=enc.mob_taggable,sort=nearest,limit=1] add enc.mob_target

# play tag sound/effects
playsound block.note_block.bell player @s ~ ~ ~
playsound block.amethyst_cluster.break player @s ~ ~ ~
playsound block.amethyst_cluster.break player @s ~ ~ ~
playsound block.amethyst_cluster.break player @s ~ ~ ~
execute at @e[tag=enc.mob_target,sort=nearest,limit=1] run particle wax_on ~ ~ ~ 1 1 1 1 30

# clear effects/tags from tagged entity
execute as @e[tag=enc.mob_target,sort=nearest,limit=1] run effect clear @s
execute as @e[tag=enc.mob_target,sort=nearest,limit=1] run tag @s add enc.mob_tagged
execute as @e[tag=enc.mob_target,sort=nearest,limit=1] run tag @s remove enc.mob_taggable

# if no more mobs need to be tagged, run end function
execute if score @s enc.mobs matches ..0 run function fennifith:encounters/game/on_round_end

# Reset the tag detection advancement
advancement revoke @s only fennifith:encounters/tag
