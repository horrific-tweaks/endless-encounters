clear @s

<% for key, item in inventory %>
<% set slot %>hotbar.<<loop.index0>><% endset %>
item replace entity @s << item.slot | default(slot) >> with << key >><< item.nbt | default({}) | dump_nbt >> << item.count | default(1) >>
<% endfor %>
