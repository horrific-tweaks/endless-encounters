# remove all settings values
scoreboard players reset * enc.player
scoreboard players reset * enc.mobs
scoreboard players reset * enc.points
scoreboard players set * enc.round 0
scoreboard players set * enc.deaths 0
scoreboard players set * enc.arrows 0

scoreboard objectives setdisplay sidebar
advancement revoke @s only fennifith:encounters/root

tag @a remove enc.player
tag @a remove enc.player_round
tag @a remove enc.winner1
tag @a remove enc.winner2
tag @a remove enc.winner3
kill @e[tag=enc.mob]
effect clear @a
gamemode adventure @a

# remove all bossbars
<% for i in range(0, config.players) %>
bossbar remove enc.bossbar<<i>>
<% endfor %>
